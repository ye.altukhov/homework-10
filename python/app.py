from fastapi import FastAPI
import redis
import random
import json
from fastapi.responses import JSONResponse
import beanstalkc


app = FastAPI()

@app.get("/")
async def root():
    return

@app.get("/get_beanstalkd")
async def get_beanstalkd():
    b = beanstalkc.Connection(host='beanstalkd', port=11300)
    job = b.reserve()
    print(job.body)
    job.delete()

@app.post("/post_beanstalkd")
async def post_beanstalkd():
    b = beanstalkc.Connection(host='beanstalkd', port=11300)
    b.put(get_customer_order())

@app.get("/get_rdb")
async def get_rdb():
    r = redis.Redis(host='redis_rdb', port=6379)
    v = r.lpop('queue')
    print(v)

@app.post("/post_rdb")
async def set_rdb():
    r = redis.Redis(host='redis_rdb', port=6379)
    r.rpush('queue', get_customer_order())

@app.get("/get_aof")
async def get_aof():
    r = redis.Redis(host='redis_aof', port=6379)
    v = r.lpop('queue')
    print(v)

@app.post("/post_aof")
async def post_aof():
    r = redis.Redis(host='redis_aof', port=6379)
    r.rpush('queue', get_customer_order())

def get_customer_order():
    return json.dumps({'cutomer':random.randint(0, 100), 'product':random.randint(0, 1000), 'quantity': random.randint(0, 10)})
