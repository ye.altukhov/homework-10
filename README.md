RUN docker-compose up

siege -c 10 --time=60S -f FILE
siege -c 25 --time=60S -f FILE
siege -c 50 --time=60S -f FILE
siege -c 100 --time=60S -f FILE

Response time "sec"

| concurrent    | rdb           | aof           | beanstalkd    |
| ------------- |:-------------:|:-------------:|:-------------:|
| 10            | 0.03          | 0.05          | 0.06          |
| 25            | 0.11          | 0.12          | 0.11          |
| 50            | 0.25          | 0.19          | 0.2           |
| 100           | 0.41          | 0.43          | 0.44          |


Transaction rate "trans/sec"

| concurrent    | rdb           | aof           | beanstalkd    |
| ------------- |:-------------:|:-------------:|:-------------:|
| 10            | 208.33        | 226.20        | 171.01        |
| 25            | 221.27        | 203.65        | 173.31        |
| 50            | 228.57        | 222.79        | 182.42        |
| 100           | 237.11        | 229.88        | 180.30        |


Longest transaction

| concurrent    | rdb           | aof           | beanstalkd    |
| ------------- |:-------------:|:-------------:|:-------------:|
| 10            |     0.13      |     0.11      |     0.16      |
| 25            |     0.31      |     0.22      |     0.31      |
| 50            |     0.45      |     0.29      |     0.40      |
| 100           |     0.51      |     0.33      |     0.54      |